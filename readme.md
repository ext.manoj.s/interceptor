# Prerequisites

Backend server should be running on localhost:6060

# Getting started

-   Clone repository

```
git clone git@gitlab.com:ext.manoj.s/interceptor.git
```

-   Install dependencies

```
npm i
```

-   Start Project

```
npm start
```
