import styled from "styled-components";

export const Wrapper = styled.div`
    padding: 1rem;

    li {
        border-bottom: 1px solid #ccc;
        padding: 0.5rem 0;
    }

    button {
        padding: 0.5rem;
        background: none;
        border: none;
        box-shadow: 0 2px 5px #ccc;
    }
`;

export const Actions = styled.div`
    display: flex;
    justify-content: space-around;
    gap: 1rem;
`;
