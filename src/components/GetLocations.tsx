import { gql, useLazyQuery, useMutation, useQuery } from "@apollo/client";

import { Actions, Wrapper } from "./styles";

const GET_LOCATION_BY_SEARCHTEXT: any = gql`
    query GET_LOCATION_BY_SEARCHTEXT($searchText: String!) {
        searchLocation(searchText: $searchText) {
            pincode
            state
            area
            state_id
        }
    }
`;

const SEND_OTP = gql`
    mutation sendOtpViaSms($mobile: String!) {
        sendOtpViaSms(mobileNumber: $mobile)
    }
`;

const GET_CANDIDATE_UNANSWERED_QUESTION = gql`
    query GetCandidateUnAnsweredQuestion($candidateId: Int!) {
        getCandidateUnAnsweredQuestion(candidateId: $candidateId) {
            totalQuestions
            totalAnsweredQuestions
            questionsByCategory
        }
    }
`;

const GetLocations = () => {
    const { loading, error, data } = useQuery(GET_LOCATION_BY_SEARCHTEXT, {
        variables: {
            searchText: "Delhi",
        },
    });

    const [getCandidateUnAnsweredQuestion] = useLazyQuery(GET_CANDIDATE_UNANSWERED_QUESTION);

    const [mutation] = useMutation(SEND_OTP);

    const triggerNetworkError = async () => {
        try {
            await mutation({
                variables: {
                    mobile: 8383099735,
                },
            });
        } catch (error) {
            // console.log(error);
        }
    };

    const triggerAuthError = () => {
        getCandidateUnAnsweredQuestion({
            variables: {
                candidateId: 1,
            },
        });
    };

    if (loading) return <h3>Loading..</h3>;
    if (error) return <h3>Something went wrong!</h3>;

    return (
        <Wrapper>
            <ul>
                {data.searchLocation.map((location: any, index: number) => (
                    <li key={index}>
                        {location.area}, <small>{location.pincode}</small>
                    </li>
                ))}
            </ul>
            <Actions>
                <button onClick={triggerNetworkError}>Trigger Network Error</button>
                <button onClick={triggerAuthError}>Trigger Auth Error</button>
            </Actions>
        </Wrapper>
    );
};

export default GetLocations;
