import { ApolloLink, ApolloClient, ApolloProvider, InMemoryCache, HttpLink, from } from "@apollo/client";
import { onError } from "@apollo/client/link/error";

import GetLocations from "./GetLocations";

const httpLink = new HttpLink({
    uri: "http://localhost:6060/graphql",
});

const errorLink = onError(({ graphQLErrors, networkError, operation }) => {
    if (graphQLErrors)
        graphQLErrors.forEach(({ message, path, extensions }) => {
            console.error(`[GraphQL error]: Query: [${operation.operationName}], Code: [${extensions!.code}], Message: ${message}, Path: ${path}`);
        });

    if (networkError) console.error(`[Network error]: ${networkError}`);
});

const loggerLink = new ApolloLink((operation, forward) => {
    console.info(`[OutGoingQuery]: Query: [${operation.operationName}]`);
    operation.setContext({ start: new Date() });
    return forward(operation).map((data: any) => {
        // Called after server responds

        const time = +new Date() - operation.getContext().start;

        // Add/Changing response things to response
        data.time = time;
        console.log(`[Response]: Query ${operation.operationName} took ${time}ms to complete`, `[Data]:`, data);
        return data;
    });
});

function App() {
    const client = new ApolloClient({
        cache: new InMemoryCache(),
        link: from([loggerLink, errorLink, httpLink]),
    });

    return (
        <ApolloProvider client={client}>
            <div className="App">
                <GetLocations />
            </div>
        </ApolloProvider>
    );
}

export default App;
